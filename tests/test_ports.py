import pytest
import socket
import os
import requests

@pytest.fixture
def api_key():
    return os.getenv('SU_API_KEY', '123456')

@pytest.fixture
def qserver_host():
    return os.getenv('QSERVER_HOST', 'localhost')

@pytest.fixture
def qserver_ports():
    p1 = int(os.getenv('QSERVER_ZMQ_CONTROL_PORT', 60615))
    p2 = int(os.getenv('QSERVER_ZMQ_INFO_PORT', 60625))
    return [p1, p2]


@pytest.fixture
def redis_host():
    return os.getenv('REDIS_HOST', 'localhost')


@pytest.fixture
def redis_port():
    return int(os.getenv('REDIS_PORT', 6379))


@pytest.fixture
def qserver_api_host():
    return os.getenv('QSERVER_API_HOST', 'localhost')


@pytest.fixture
def qserver_api_port():
    return int(os.getenv('QSERVER_API_PORT', 60610))


def is_port_open(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print(f"\nChecking {host}:{port}")
        return s.connect_ex((host, port)) == 0


def test_redis_port(redis_host, redis_port):
    assert is_port_open(redis_host, redis_port)

def test_qserver_ports(qserver_host, qserver_ports):
    for port in qserver_ports:
        assert is_port_open(qserver_host, port)

def test_qserver_api_port(qserver_api_host, qserver_api_port):
    assert is_port_open(qserver_api_host, qserver_api_port)

def test_api_status(qserver_api_host, qserver_api_port, api_key):
    print(f"\nChecking http://{qserver_api_host}:{qserver_api_port}/api/status")
    response = requests.get(f"http://{qserver_api_host}:{qserver_api_port}/api/status",
                            headers={'Authorization': f'ApiKey {api_key}'})
    assert response.status_code == 200
