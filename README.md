# rock-it_qapi

`rock-it_qapi` is a Docker container designed to hold the HTTP API to the queue server.
It is part of a containerized deployment for a Bluesky control system.

Simple implementation with single-user mode for demonstration and development purposes.

## Features

- Contains the queueserver API.
- Designed for containerized deployment.
- This is the prototype for containerized bluesky services as part of the
`ROCK-IT Starterpack`.

## Getting Started

To get a local copy up and running:

1. Pull and run the container from the registry.

`docker run --name rock-it_qapi -d registry.hzdr.de/rock-it/wp2/rock-it_qapi:latest`

### Prerequisites

- Docker

## Usage

Use this container as part of your Bluesky control system deployment. 
It is designed to work with other containers in the `rock-it` project.

## Contributing

This container is a prototype and we are encouraging contributions from other ROCK-IT
members. If you would like to contribute, please follow the steps below:

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request