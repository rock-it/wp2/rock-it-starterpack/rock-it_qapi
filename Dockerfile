FROM docker.io/python:3.11

COPY ./pyproject.toml ./pyproject.toml

ENV POETRY_VIRTUALENVS_CREATE=false

RUN python3 --version
RUN python3 -m venv venv
RUN /bin/bash -c "source venv/bin/activate"
RUN pip install poetry
RUN poetry install

RUN mkdir tests
RUN mkdir scripts
RUN mkdir config
RUN mkdir startup
RUN mkdir logs

COPY tests/ tests
COPY scripts/ scripts
COPY config/qapi/ config
COPY startup/ startup

RUN chmod +x startup/startup.sh

VOLUME /logs
EXPOSE 60610

CMD ["startup/startup.sh"]