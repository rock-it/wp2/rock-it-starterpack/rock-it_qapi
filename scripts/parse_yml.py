"""
****************************************************************************************
* Title: parse_yml.py
* Project: ROCK-IT Containers
* This script is used to parse a yaml file and return the value of a given key.
****************************************************************************************
"""

import yaml
import sys
import re


def sanitize_input(input_string):
    input_string = input_string.lower()
    if not re.match("^[a-z0-9_]*$", input_string):
        raise ValueError(f"Invalid input: {input_string}. Input should be alphanumeric "
                         f"and can include underscores.")

    return input_string


def parse_yaml(yaml_file, *keys):
    with open(yaml_file, 'r') as f:
        data = yaml.safe_load(f)

    # Navigate through nested keys
    for key in keys:
        sanitized_key = sanitize_input(key)
        data = data[sanitized_key]

    return data


if __name__ == "__main__":
    file = sys.argv[1]
    data_keys = sys.argv[2:]
    print(parse_yaml(file, *data_keys))
