#!/bin/bash

export SU_API_KEY=${SU_API_KEY:-'123456'}

export QSERVER_HOST=${QSERVER_HOST:-'localhost'}
export QSERVER_CONTROL_PORT=${QSERVER_CONTROL_PORT:-'60615'}
export QSERVER_INFO_PORT=${QSERVER_INFO_PORT:-'60625'}
export QSERVER_API_HOST=${QSERVER_API_HOST:-'localhost'}
export QSERVER_API_PORT=${QSERVER_API_PORT:-'60610'}


# If SU_API_KEY is it's default value, print a warning
if [ "${SU_API_KEY}" == '123456' ]; then
    echo "WARNING: SU_API_KEY is set to its default value. Please set it to a secure value." | tee -a /logs/qapi.log
fi

QSERVER_HTTP_SERVER_CONFIG=/config/http/config.yml uvicorn bluesky_httpserver.server:app --host ${QSERVER_API_HOST} --port ${QSERVER_API_PORT} | tee -a /logs/qapi.log